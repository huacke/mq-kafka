#!/bin/sh -e


FILENAME="apache-maven-${MAVEN_VERSION}-bin.tar.gz"
url=http://mirrors.cloud.tencent.com/apache/maven/maven-3/${MAVEN_VERSION}/binaries/${FILENAME}

echo "Downloading maven from $url"
wget  -qO /tmp/${FILENAME}  ${url}